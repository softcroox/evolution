<?php
include('app.php');
// INDEFICIA LA VISTA DEL MENU
function menu()
{
  if(isset($_SESSION['usr']))
  {
    echo '<header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="#" class="logo"><b>Evo<span>lution</span></b></a>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->

        <!--  notification end -->
      </div>
      <div class="top-menu">
        <ul class="nav pull-left top-menu">
        <li><a class="logout" href="?rt=perfil"> <i class="fa fa-user"></i></a></li>
          <br>';
          include('views/content/list.notifications.money.php');
         echo '
        </ul>
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="?rt=exit"> CERRAR SESSION</a></li>
        </ul>
      </div>
    </header>';
  }else
  {
    echo '

    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        
      </div>
      <!--logo start-->
      <a href="#" class="logo"><b>Evo<span>lution</span></b></a>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->

        <!--  notification end -->
      </div>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="?rt=index">Inicio</a>
          </li>
          <li><a class="logout" href="?rt=register">Registrate</a></li>
        </ul>
      </div>
    </header>

    ';

  }

}
// IDENTIFICA LAS RUTAS DEL SITIO
function rout()
{

  if(isset($_GET['rt']))
  {

    $rt=$_GET['rt'];
    if($rt=='index'){include('views/content/login.view.php');}
    else if($rt=='login'){include('views/content/login.view.php');}
    else if($rt=='register'){include('views/content/register.view.php');}

    else if($rt=='exit')
    {
      session_destroy();
      header("location: ../evolution/?rt=index");
    }
    else
    {
        echo 'No sabes donde empezar?';
    }

  }
  if(isset($_SESSION['usr']) && $rt=="perfil")
  {
    include('views/panel/perfil.app.php');
  }
  else if(isset($_SESSION['usr']))
  {
    include('views/panel/panel.app.php');
  }
}
?>
