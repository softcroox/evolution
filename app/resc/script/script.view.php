<script src="app/bootstrap/lib/jquery/jquery.min.js"></script>
<script src="app/bootstrap/lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="app/bootstrap/lib/jquery.backstretch.min.js"></script>
<script>
  $.backstretch("app/bootstrap/img/login-bg.jpg", {
    speed: 500
  });
  function LoadName(nickname)
  {
    
  }
  function LoadPhone(phone)
  {

  }
  function LoadMail(mail)
  {

  }
  function LoadMunicipio(id)
  {
    
    var parametros={"id":id};
     $.ajax({
          data:  parametros,
          url:   'app/controllers/municipioController.php',
          type:  'post',
          beforeSend: function ()
          {
                  $("#munic_r").html("<option>Cargando...</option>");
          },
          success:  function (response) 
          {
                  $("#munic_r").html(response);

          }
  });
  }
function elimiarTarea(id)
{

  var parametros = {
          "id":id,
          "funt": "elimiartarea"
  };

  $.ajax({
          data:  parametros,
          url:   'app/controllers/tareasController.php',
          type:  'post',
          beforeSend: function ()
          {
                  $("#bbody").html("Verificando...");
          },
          success:  function (response) {
                  $("#bbody").html(response);

          }
  });
}
function editarTarea(id)
{
  var parametros = {
          "id":id,
          "funt": "editartarea"
  };

  $.ajax({
          data:  parametros,
          url:   'app/controllers/tareasController.php',
          type:  'post',
          beforeSend: function ()
          {
                  $("#myModal").modal();
          },
          success:  function (response) {
                  $("#myModal").html(response);

          }
  });
}
function crearTarea()
{
  var parametros = {
          "funt": "creartarea"
  };

  $.ajax({
          data:  parametros,
          url:   'app/controllers/tareasController.php',
          type:  'post',
          beforeSend: function ()
          {
                  $("#myModal").modal();
          },
          success:  function (response) {
                  $("#myModal").html(response);

          }
  });
}
function vrf()
{
  var usr = $('#usr_l').val();
  var psw = $('#psw_l').val();

  if(usr!=''&& psw!='')
  var parametros = {
          "usr" : usr,
          "psw": psw,
          "funt": "vrlogin"
  };

  $.ajax({
          data:  parametros,
          url:   'app/controllers/loginController.php',
          type:  'post',
          beforeSend: function ()
          {
                  $("#resultado").html("Verificando...");
          },
          success:  function (response) {
                  $("#resultado").html(response);
          }
  });
}
function CrearTarea()
{
  var nombre = $('#nombre').val();
  var prioridad = $('#prioridad').val();
  var datevence = $('#datevence').val();

  if(nombre!=''&& prioridad!=''&& datevence!='')
  var parametros = {
          "nombre" : nombre,
          "prioridad": prioridad,
          "datevence":datevence,
          "funt": "registertarea"
  };

  $.ajax({
          data:  parametros,
          url:   'app/controllers/tareasController.php',
          type:  'post',
          beforeSend: function ()
          {
                  $('#nombre').val('');
                  $('#datevence').val('');
                  $("#bbody").html("Verificando...");
          },
          success:  function (response) {
                  $("#bbody").html(response);

          }
  });
}
function EditarTarea()
{
  var id = $('#id_e').val();
  var nombre = $('#nombre_e').val();
  var prioridad = $('#prioridad_e').val();
  var datevence = $('#datevence_e').val();

  if(nombre!=''&& prioridad!=''&& datevence!='')
  var parametros = {
          "id":id,
          "nombre" : nombre,
          "prioridad": prioridad,
          "datevence":datevence,
          "funt": "EditarTarea"
  };

  $.ajax({
          data:  parametros,
          url:   'app/controllers/tareasController.php',
          type:  'post',
          beforeSend: function ()
          {
                  $('#nombre').val('');
                  $('#datevence').val('');
                  $("#bbody").html("Verificando...");
          },
          success:  function (response) {
                  $("#bbody").html(response);

          }
  });
}
function vrfTarea()
{
  var nombre = $('#nombre').val();
  var prioridad = $('#prioridad').val();
  var datevence = $('#datevence').val();
  var asignacion = $('#asignacion').val();
  var asignar = $('#asignar').val();

  if(nombre!='' && prioridad!='' && datevence!='')
  {
    $("#btCrearTarea").html('<button   data-dismiss="modal" class="btn btn-theme" onclick="CrearTarea();" type="button">Guardar</button>');
  }

  else
  {
    $("#btCrearTarea").html('<button   data-dismiss="modal" disabled="true" class="btn btn-theme" onclick="CrearTarea();" type="button">Guardar</button>');

  }

  
 if( document.getElementById("asignacion").checked == true)
   {
    document.getElementById("asignar").disabled=false;
   }
   else
   {
    document.getElementById("asignar").disabled=true;
   }
  
 }
  
function vrfTarea_e()
{
  var nombre = $('#nombre_e').val();
  var prioridad = $('#prioridad_e').val();
  var datevence = $('#datevence_e').val();

  if(nombre!='' && prioridad!='' && datevence!='')
  {
    $("#btEdutarTarea_e").html('<button   data-dismiss="modal" class="btn btn-theme" onclick="EditarTarea();" type="button">Editar</button>');
  }

  else
  {
    $("#btEdutarTarea_e").html('<button   data-dismiss="modal" disabled="true" class="btn btn-theme" onclick="EditarTarea();" type="button">Editar</button>');

  }

}

function RegistrarUsuario()
{
  var usr = $('#usr_r').val();
  var pass1 = $('#pass1_r').val();
  var pass2 = $('#pass2_r').val();
  var email = $('#email_r').val();
  var prof = $('#prof_r').val();
  if(usr!=''&& pass1!=''&& pass2!=''&& email!=''&& prof!=''|| pass1==pass2)

  alert('Registrando');
  var parametros = {
          "usr" : usr,
          "pass1": pass1,
          "email":email,
          "prof":prof,
          "funt": "registrarusuario"
  };

  $.ajax({
          data:  parametros,
          url:   'app/controllers/registerController.php',
          type:  'post',
          beforeSend: function ()
          {
            $('#usr_r').val('');
            $('#pass1_r').val('');
            $('#pass2_r').val('');
            $('#email_r').val('');
            $('#prof_r').val('');
            $("#result_r").html("Verificando...");
          },
          success:  function (response)
          {
                  $("#result_r").html(response);

          }
  });

}

</script>
<script src="app/bootstrap/lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script type="text/javascript" src="app/bootstrap/lib/bootstrap-fileupload/bootstrap-fileupload.js"></script>
  <script type="text/javascript" src="app/bootstrap/lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="app/bootstrap/lib/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="app/bootstrap/lib/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="app/bootstrap/lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript" src="app/bootstrap/lib/bootstrap-daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="app/bootstrap/lib/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script src="app/bootstrap/lib/advanced-form-components.js"></script>
