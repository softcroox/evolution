<div id="login-page">
    <div class="container">
      <div class="col-lg-6 col-md-6 col-sm-12" >
        <h2 class="form-login-heading">Evolution 0.1</h2>
        <br><br><br><br><br>
        <div class="showback col-lg-16">
            <h4><i class="fa fa-angle-right"></i> Repositorio Test</h4>
            <p>Desarrollado por:softcroox@gmail.com</p>
            <p>Ing : Edgar Niebles Castaño </p>

            <!-- Button trigger modal -->
            <button class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModal">
              Ver Detalles...
              </button>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">https://bitbucket.org/evogit/todo-items.git</h4>
                  </div>
                  <div class="modal-body">
                    <p>
                      Desarrollar una aplicación que permitirá a los usuarios crear y gestionar varias tareas.
                      La aplicación debe almacenar los siguientes datos en una estructura de base de datos
                      correctamente normalizada:
        </p><br><p>
            Nombre de la tarea.
            Prioridad de la tarea.
            Fecha de vencimiento.
        </p><br><p>
            Las tareas serán de propiedad/asignadas a un único usuario (el creador de la tarea) y deben alertar
            al usuario que alguna tarea se va a vencer a través de la interfaz gráfica al iniciar sesión.
        </p><br><p>
            Implementar una vista única con una interfaz tipo "modal" para añadir los elementos pendientes y
            una interfaz de lista que permite al usuario gestionar (eliminar / editar) las tareas pendientes.
        </p><br><p>
            La interfaz puede ser implementada utilizando cualquier libreria de javascript (Jquery, Yui, backbone, Bootstrap ..etc)
            y hay puntos extras por programación orientada a objectos en el cliente, ademas de tecnologias como AJAX.
        </p><br><p>
            El sistema debe requerir autenticación/autorización y sólo hará una lista y/o permitir la edición de las tareas
            pendientes al usuario actualmente conectado.
            La autenticación debe hacerse a través de combinación de correo electrónico/contraseña,
        </p><br><p>
            Ademas se debe desarrollar una interfaz (backend) simple para añadir usuarios a la aplicación, la creación de usuarios
            debe ser proporcionada mediante correo electrónico, contraseña, confirmación de contraseña.
        </p><br><p>
            Se espera una completa funcionalidad de las características mencionadas anteriormente,
            así como la interfaz del usuario (frontend).
        </p><br><p>
            Sólo los navegadores más recientes deben ser soportados.
        </p><br><p>
            La elección de que tecnología usar se deja al desarrollador, pero se requieren todos los archivos del backend y el frontend.
            Cualquier framework de PHP MVC se puede utilizar.
        </p><br><p>
            Se tendrán en cuenta los siguientes criterios a la hora de revisar la aplicación:
        </p><br><p>
                Comprensión de los requisitos.
                Calidad de código (limpio, comentarios, programación orientada a objetos, el uso adecuado de patrones MVC).
                El uso de las tecnologías disponibles.
                Las estructuras de datos.
        </p><br><p>
            Por favor, proporcione un vínculo al repositorio "github/bitbucket" con el código de la aplicación.
            El repositorio debe incluir instrucciones para configurar y poner en marcha la aplicación

                    </p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  </div>
                </div>
              </div>
            </div>
          </div>


    </div>
  </div>
