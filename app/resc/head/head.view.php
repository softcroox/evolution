<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Evolution</title>
  <link href="app/bootstrap/img/favicon.png" rel="icon">
  <link href="app/bootstrap/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link href="app/bootstrap/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="app/bootstrap/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="app/bootstrap/css/style.css" rel="stylesheet">
  <link href="app/bootstrap/css/style-responsive.css" rel="stylesheet">

  <link href="app/bootstrap/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="app/bootstrap/lib/bootstrap-fileupload/bootstrap-fileupload.css" />
  <link rel="stylesheet" type="text/css" href="app/bootstrap/lib/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="app/bootstrap/lib/bootstrap-daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" type="text/css" href="app/bootstrap/lib/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="app/bootstrap/lib/bootstrap-datetimepicker/datertimepicker.css" />
</head>
