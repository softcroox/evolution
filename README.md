Evolution 0.1


Desarrollado por: Edgar Alberto Niebles Casta�o
Correo:softcroox@gmail.com



Instalaci�n Repositorio.

Descarga el repositorio de �evolution� en  htdocs en tu Xampp

Asi:

C:\xampp\htdocs\evolution
				|---.git/..
				|---app/
				|	 |------bootstrap/
				|	 |------controllers/ -->{ Archivos control Sistema}
				|	 |------resc/	-->{Archivos de recursos}
				|	 |------views/ -->	{Archivos de vistas}
				|	 |------app.php* -->  { Archivo de conexion db; }
				|	 |------routes.php* -->{ Archivo de Rutas del Sitio }
				|	 �
				|----	index.php*
				|---- evolution.sql*
				|---- README.md*


Instalar Base de datos
Crea una nueva base de datos en mysql llamada: �evolution�  y importa el archivo �evolution.sql� 

Y modifica el archivo de conexi�n �app.php*�


Tabla usr_user


Tabla usr_tarea

prueba que hace parte del proceso de selecci�n



Desarrollar una aplicaci�n que permitir� a los usuarios crear y gestionar varias tareas. La aplicaci�n debe almacenar los siguientes datos en una estructura de base de datos correctamente normalizada:

Nombre de la tarea.
Prioridad de la tarea.
Fecha de vencimiento.
Las tareas ser�n de propiedad/asignadas a un �nico usuario (el creador de la tarea) y deben alertar al usuario que alguna tarea se va a vencer a trav�s de la interfaz gr�fica al iniciar sesi�n.

Implementar una vista �nica con una interfaz tipo "modal" para a�adir los elementos pendientes y una interfaz de lista que permite al usuario gestionar (eliminar / editar) las tareas pendientes.

La interfaz puede ser implementada utilizando cualquier libreria de javascript (Jquery, Yui, backbone, Bootstrap ..etc) y hay puntos extras por programaci�n orientada a objectos en el cliente, ademas de tecnologias como AJAX.

El sistema debe requerir autenticaci�n/autorizaci�n y s�lo har� una lista y/o permitir la edici�n de las tareas pendientes al usuario actualmente conectado. La autenticaci�n debe hacerse a trav�s de combinaci�n de correo electr�nico/contrase�a,

Ademas se debe desarrollar una interfaz (backend) simple para a�adir usuarios a la aplicaci�n, la creaci�n de usuarios debe ser proporcionada mediante correo electr�nico, contrase�a, confirmaci�n de contrase�a.

Se espera una completa funcionalidad de las caracter�sticas mencionadas anteriormente, as� como la interfaz del usuario (frontend).

S�lo los navegadores m�s recientes deben ser soportados.

La elecci�n de que tecnolog�a usar se deja al desarrollador, pero se requieren todos los archivos del backend y el frontend. Cualquier framework de PHP MVC se puede utilizar.

Se tendr�n en cuenta los siguientes criterios a la hora de revisar la aplicaci�n:

Comprensi�n de los requisitos.
Calidad de c�digo (limpio, comentarios, programaci�n orientada a objetos, el uso adecuado de patrones MVC).
El uso de las tecnolog�as disponibles.
Las estructuras de datos.
Por favor, proporcione un v�nculo al repositorio "github/bitbucket" con el c�digo de la aplicaci�n. El repositorio debe incluir instrucciones para configurar y poner en marcha la aplicaci�n



