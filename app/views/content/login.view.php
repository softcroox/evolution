<div id="login-page">
    <div class="container">
      <form class="form-login" action="app/controllers/loginController.php" method="post">
        <h2 class="form-login-heading">Acceder</h2>
        <div class="login-wrap">
          <input type="text" id="usr_l"  name="usr_l" class="form-control" placeholder="USUARIO" autofocus>
          <br>
          <input type="password" onkeyup="vrf();" id="psw_l" name="psw_l" class="form-control" placeholder="CONTRASEÑA">
          <input type="hidden" id="funt" name="funt" class="form-control" value="startlogin">
          <label class="checkbox">
            <input type="checkbox" value="remember-me"> Recordar Datos.
            <span class="pull-right">
            <a data-toggle="modal" href="#myModal"> Recuperar Cuenta</a>
            </span>
            </label>
            <div id="resultado" name="resultado"><button class="btn btn-theme btn-block " disabled="true" href="index.html" type="submit"><i class="fa fa-lock"></i> -INICIAR</button></div>

          <hr>
        </div>

        <!-- Modal -->
        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Forgot Password ?</h4>
              </div>
              <div class="modal-body">
                <p>Enter your e-mail address below to reset your password.</p>
                <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
              </div>
              <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                <button class="btn btn-theme" type="button">Submit</button>
              </div>
            </div>
          </div>
        </div>
        <!-- modal -->
      </form>
    </div>
  </div>
