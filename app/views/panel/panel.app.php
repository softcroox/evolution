<?php
$db = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
$id=$_SESSION['id'];
$sql="SELECT * FROM usr_tarea where id_usr_asig=$id";
$result = $db->query($sql);
$tabla='';
while ($row = $result->fetch_array(MYSQLI_ASSOC))
{
  $horaInicio = date("Y/m/d");
  $horaTermino = $row['datevence'];
  $dias = (strtotime($horaInicio)-strtotime($horaTermino))/86400;
  $interval = abs($dias); $dias = floor($dias);
  $tabla= $tabla.'
  <tr>
      <td>
        <a href="basic_table.html#">'.$row['nombre'].'</a>
      </td>
      <td class="hidden-phone">'.$row['prioridad'].'</td>
      <td>'.$row['datevence'].'</td>
      <td><span class="label label-info label-mini"> Dias: '.$interval.'</span></td>
      <td>
        <button class="btn btn-primary btn-xs" onclick="editarTarea('.$row['id'].');"><i class="fa fa-pencil"></i></button>
        <button class="btn btn-danger btn-xs" onclick="elimiarTarea('.$row['id'].');"><i class="fa fa-trash-o "></i></button>
      </td>
    </tr>';
  }
?>

<br><br>
<br><br>

<div class="col-md-12">
            <div class="content-panel">
              <ul class="nav pull-right top-menu">
                <li><a class="logout" onclick="crearTarea();" data-toggle="modal" >CREAR TAREA NUEVA</a></li>
                <li><a class="logout" onclick="CambiarVista();" data-toggle="modal" >CAMBIAR VISTA</a></li>
                <li><a class="logout" onclick="Evolution();" data-toggle="modal" >EVOLUTION</a></li>
              </ul>
              <h4><i class="fa fa-angle-right"></i> Tareas Programadas</h4><hr>
              <table class="table table-striped table-advance table-hover">




                <thead>
                  <tr>
                    <th><i class="fa fa-bullhorn"></i> NOMBRE</th>
                    <th class="hidden-phone"><i class="fa fa-question-circle"></i> PRIORIDAD</th>
                    <th><i class="fa fa-bookmark"></i> FECHA DE VENCIMIENTO</th>
                    <th><i class=" fa fa-edit"></i> TIEMPO RESTANTE </th>
                    <th></th>
                  </tr>
                </thead>
                <tbody id="bbody">
                  <?php echo $tabla;?>
                </tbody>
              </table>
            </div>
            <!-- /content-panel -->
          </div>


          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Crear Nueva Tarea</h4>
                </div>
                <div class="modal-body">

                  <p>Asigna Un Nombre a la Tarea</p>
                  <input type="text" onchange="vrfTarea();" id="nombre" name="nombre" placeholder="NOMBRE TAREA A REALIZAR" autocomplete="off" class="form-control placeholder-no-fix">
                  <br>
                  <p>Seleciona  Prioridad </p>
                  <select onchange="vrfTarea();" id="prioridad" name="prioridad" class="form-control" required>
                  <option value="ALTA">ALTA</option>
                  <option value="MEDIA">MEDIA</option>
                  <option value="BAJA">BAJA</option>
                  <option value="NINGUNA">NINGUNA</option>
                  </select>
                  <br>
                  <input onchange="vrfTarea();" id="datevence" name="datevence" size="16" type="text" value="" readonly="FECHA DE VENCIMIENTO" class="form_datetime form-control">
                </div>
                <div class="modal-footer">
                  <button data-dismiss="modal" class="btn btn-default" type="button">Cacelar</button>
                  <div id="btCrearTarea" name="btCrearTarea">
                    <button   data-dismiss="modal" disabled="true" class="btn btn-theme" onclick="CrearTarea();" type="button">Guardar</button>
                  </div>
                </div>

              </div>
            </div>
          </div>
